<?php namespace verilion\vcms;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class SearchController extends \Controller {

    public function performSearch()
    {
        if (Input::get('q') != null)
        {
            $searchterm = '%' . Input::get('q') . '%';

            if ((Session::get('lang') == "en") || (Session::get('lang') == null))
            {
                $results = Page::whereRaw('page_title ilike ? or page_content ilike ?',
                    array($searchterm, $searchterm))
                    ->get();
            } else
            {
                $results = Page::whereRaw('page_title_fr ilike ? or page_content_fr ilike ?',
                    array($searchterm, $searchterm))
                    ->get();
            }

            return View::make('search')

                ->with('page_title', 'Request a Quote')
                ->with('meta_tags', '')
                ->with('meta', '')
                ->with('searchterm', $searchterm)
                ->with('results', $results);
        } else
        {
            return View::make('search')
                ->with('results', array());
        }
    }
}
