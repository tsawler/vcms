<?php namespace verilion\vcms;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Request;


class GalleryController extends \Controller {

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    public function getAllItems()
    {
        return View::make('vcms.gallery')
            ->with('title', 'Gallery');
    }

    /**
     * List all galleries
     *
     * @return mixed
     */
    public function getAllGalleries()
    {
        $galleries = Gallery::orderby('gallery_name')->paginate(16);

        return View::make('vcms::admin.galleries-list-all')
            ->with('allgalleries', $galleries)
            ->with('page_name', '');
    }


    /**
     * Display gallery for edit, along with all entries
     */
    public function getEditGallery()
    {
        if (Input::get('id') > 0)
        {
            $gallery = Gallery::find(Input::get('id'));
            $items = GalleryItem::where('gallery_id', '=', $gallery->id)->orderby('item_name')->get();
        } else
        {
            $gallery = new Gallery;
            $items = null;
        }


        return View::make('vcms::admin.gallery-edit-gallery')
            ->with('gallery', $gallery)
            ->with('items', $items)
            ->with('gallery_id', Input::get('id'));
    }


    /**
     * Save edited gallery
     *
     * @return mixed
     */
    public function postEditGallery()
    {
        $gallery_id = Input::get('gallery_id');

        if ($gallery_id > 0)
        {
            $gallery = Gallery::find($gallery_id);
        } else
        {
            $gallery = new Gallery;
        }

        $gallery->gallery_name = Input::get('gallery_name');
        if (Config::get('vcms::use_french'))
        {
            $gallery->gallery_name_fr = Input::get('gallery_name_fr');
        }
        $gallery->active = Input::get('active');
        $gallery->save();

        return Redirect::to('/admin/galleries/gallery?id=' . $gallery->id)
            ->with('message', 'Changes saved');
    }


    /**
     * Save Gallery Item
     *
     * @return mixed
     */
    public function postSaveItem()
    {
        // get the file
        $file = Input::file('image_name');
        $gallery_id = Input::get('gallery_id');
        $id = Input::get('gallery_item_id');

        if ((Input::hasFile('image_name') == false) && ($id == 0))
        {
            return Redirect::to('/admin/galleries/gallery?id=' . $gallery_id)
                ->with('error', 'You must include an image file!')
                ->withInput();
        }

        if (Input::hasFile('image_name'))
        {
            $destinationPath = base_path() . '/public/galleries/' . $gallery_id . "/";

            $filename = $file->getClientOriginalName();
            $upload_success = Input::file('image_name')->move($destinationPath, $filename);
            if(! \File::exists($destinationPath."thumbs")) {
                \File::makeDirectory($destinationPath."thumbs");
            }
            //dd($destinationPath."thumbs/" . $filename);
            $thumb_img = \Image::make($destinationPath. $filename);
            $height = $thumb_img->height();
            $width = $thumb_img->width();


            if (($height < Config::get('vcms::min_img_height')) || ($width < Config::get('vcms::min_img_width'))) {
                return Redirect::to('/admin/galleries/gallery?id='.$gallery_id)
                    ->with('error','Your image is too small. It must be at least '
                    . Config::get('vcms::min_img_width')
                    . ' pixels wide, and '
                    . Config::get('vcms::min_img_height')
                    . ' pixels tall!');
                \File::delete($destinationPath.$filename);
                exit;
            }

            $thumb_img->fit(Config::get('vcms::thumb_size'),Config::get('vcms::thumb_size'))
                ->save($destinationPath."thumbs/".$filename);

            unset($thumb_img);
            $img = \Image::make($destinationPath.$filename);

            $width = $img->width();
            if (($width > Config::get('vcms::max_img_width')) || ($height > Config::get('vcms::max_image_height'))) {
                // this image is very large; we'll need to resize it.
                $img = $img->fit(Config::get('vcms::max_img_width'), Config::get('vcms::max_img_height'));
                $img->save();
            }

            if ($upload_success)
            {
                if ($id > 0)
                {
                    $item = GalleryItem::find($id);
                } else
                {
                    $item = new GalleryItem;
                }
                $item->item_name = Input::get('item_name');
                $item->item_description = Input::get('item_description');
                if (Input::has('item_name_fr'))
                {
                    $item->item_name_fr = Input::get('item_name_fr');
                    $item->item_description_fr = Input::get('item_description_fr');
                }
                $item->active = Input::get('active');
                $item->image_name = $filename;
                $item->gallery_id = $gallery_id;
                $item->save();

                return Redirect::to('/admin/galleries/gallery?id=' . $gallery_id);

            } else
            {
                return Redirect::to('/admin/galleries/gallery?id=' . $gallery_id)
                    ->with('error', 'There was an error with your submission!');
            }
        } else
        {
            $item = GalleryItem::find($id);
            $item->item_name = Input::get('item_name');
            $item->item_description = Input::get('item_description');
            if (Input::has('item_name_fr'))
            {
                $item->item_name_fr = Input::get('item_name_fr');
                $item->item_description_fr = Input::get('item_description_fr');
            }
            $item->active = Input::get('active');
            $item->gallery_id = $gallery_id;
            $item->save();

            return Redirect::to('/admin/galleries/gallery?id=' . $gallery_id)
                ->with('message', 'Changes saved');
        }
    }


    /**
     * Delete a gallery
     *
     * @return mixed
     */
    public function getDeleteGallery()
    {
        $item = Gallery::find(Input::get('id'));
        $item->delete();

        return Redirect::to('/admin/galleries/all-galleries')
            ->with('message', 'Gallery deleted');
    }


    /**
     * Delete gallery item
     *
     * @return mixed
     */
    public function getDeleteItem()
    {
        $item = GalleryItem::find(Input::get('id'));
        $gallery_id = $item->gallery_id;
        $image_name = $item->image_name;
        \File::delete(base_path().'/public/galleries/' . $gallery_id . "/".$image_name);
        \File::delete(base_path().'/public/galleries/' . $gallery_id . "/thumbs/".$image_name);
        $item->delete();

        return Redirect::to('/admin/galleries/gallery?id=' . Input::get('gallery_id'))
            ->with('message', 'Item deleted');
    }


    /**
     * Retrieve gallery item as JSON
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRetrieveItem()
    {
        $id = Input::get('id');
        $item = GalleryItem::find($id);
        $eArray = array();
        $eArray[] = array(
            'id'                  => $item->id,
            'item_name'           => $item->item_name,
            'item_description'    => $item->item_description_fr,
            'item_name_fr'        => $item->item_name_fr,
            'item_description_fr' => $item->item_description_fr,
            'active_yn'           => $item->active,
            'image_name'          => $item->image_name,
            'gallery_id'          => $item->gallery_id,
        );

        return Response::json($eArray);
    }

}
