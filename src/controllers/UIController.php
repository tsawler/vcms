<?php namespace verilion\vcms;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class UIController extends \Controller {

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }


    /**
     * Expanding admin left nav
     *
     * @return string
     */
    public function menuUp()
    {
        Session::put('mini-navbar', 'true');
        return 'true';
    }


    /**
     * Collapsing admin left nav
     * @return string
     */
    public function menuDown()
    {
        Session::forget('mini-navbar');
        return 'true';
    }

}
