<?php namespace verilion\vcms;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;

class LanguageController extends \Controller {

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }


    /**
     * Change language
     *
     * @return mixed
     */
    public function getChangeLanguage()
    {
        if (Session::has('lang'))
            Session::forget('lang');

        if (Input::get('lang') == "en")
        {
            Session::put('lang', 'en');
            App::setLocale('en');
        } else
        {
            Session::put('lang', 'fr');
            App::setLocale('fr');
        }

        return Redirect::to(Input::get('url'));
    }

}