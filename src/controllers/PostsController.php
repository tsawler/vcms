<?php namespace verilion\vcms;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class PostsController extends \Controller {

    /**
     * Show posts for blog
     *
     * @param null $s
     * @param null $year
     * @param null $month
     * @return mixed
     */
    public function index($s = null, $year = null, $month = null)
    {
        // get the current page
        $slug = Request::segment(2);
        $blog = Blog::where('slug', '=', $slug)->firstOrFail();

        $temp_date = strtotime($year . "-" . $month . "-01");
        $upper_limit = date("Y-m-d", strtotime("+1 month", $temp_date));
        if ($month != null)
        {
            $lower_limit = date("Y-m-d", strtotime("-1 day", $temp_date));
            $posts = BlogPost::where('blog_id', '=', $blog->id)
                ->where('active', '=', '1')
                ->where('post_date', '<', $upper_limit)
                ->where('post_date', '>', $lower_limit)
                ->paginate(10);
        } else
        {
            $posts = BlogPost::where('blog_id', '=', $blog->id)
                ->where('active', '=', '1')
                ->where('created_at', '<', $upper_limit)
                ->paginate(10);
        }

        // get the archives
        $archives = BlogPost::archives();

        return View::make('vcms.blog')
            ->with('blog', $blog)
            ->with('posts', $posts)
            ->with('archives', $archives);
    }


    /**
     * Delete post from blog
     *
     * @return mixed
     */
    public function getDeletePost()
    {
        $id = Input::get('id');
        $blog_id = Input::get('bid');
        $post = BlogPost::find($id);
        $post->delete();

        return Redirect::to('/admin/blogs/blog?id=' . $blog_id);
    }


    /**
     * Show all posts for blog in admin
     *
     * @return mixed
     */
    public function getAllPosts()
    {
        $posts = BlogPost::orderby('post_date')->get();

        return View::make('vcms::admin.blogs-all-posts')
            ->with('posts', $posts);
    }


    /**
     * RSS feed for blog
     *
     * @return \Illuminate\Http\Response
     */
    public function rss()
    {
        $feed = Rss::feed('2.0', 'UTF-8');
        $feed->channel(array(
            'title'       => Config::get('app.rss_title'),
            'description' => Config::get('app.rss_description'),
            'link'        => URL::current()
        ));
        $posts = Post::where('status', '=', Post::APPROVED)
            ->where('in_rss', '=', true)
            ->orderBy('published_date', 'desc')
            ->take(10)
            ->remember(525949)
            ->get();
        foreach ($posts as $post)
        {
            $feed->item(array(
                    'title'       => $post->title,
                    'description' => $post->summary,
                    'link'        => 'http://www.dogearedpress.ca/' . $post->slug)
            );
        }

        return Response::make($feed, 200, array('Content-Type', 'application/rss+xml'));

    }

    /**
     * Save edit post (called via ajax)
     */
    public function postEditInPlace()
    {
        $validator = Validator::make(
            Input::all(),
            array(
                'title'   => 'required|min:2|unique:' . Config::get('vcms::blog_posts_table') . ',title,' . Input::get('post_id'),
                //'published_date' => 'required',
                'content' => 'required|min:2')
        );

        if ($validator->passes())
        {
            $post = BlogPost::find(Input::get('post_id'));
            if ((Session::has('lang')) && (Session::get('lang') == 'fr'))
            {
                $post->title_fr = trim(Input::get('title'));
                $post->content_fr = Input::get('content');
            } else
            {
                $post->title = trim(Input::get('title'));
                $post->content = Input::get('content');
                $post->slug = Str::slug(trim(Input::get('title')));
            }

            $post->save();
            Cache::flush();

            return "Post updated successfully";
        } else
        {
            $m = "The following errors occurred:";
            $messages = $validator->messages();
            var_dump($messages);

            return $m;
        }
    }

}
