<?php namespace verilion\vcms;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class AdminController extends \Controller {

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }


    /**
     * Display dashboard
     *
     * @return mixed
     */
    public function getDashboard()
    {
        return View::make('vcms::admin.index');
    }


    /**
     * Display unauthorized access page
     *
     * @return mixed
     */
    public function get403()
    {
        return Response::view('vcms::admin.403', array(), 403);
    }

}