<?php namespace verilion\vcms;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Request;


class FaqController extends \Controller {

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }


    /**
     * Show all faqs
     *
     * @return mixed
     */
    public function showFaqs()
    {
        if (Session::has('lang') && (Session::get('lang') == 'fr'))
        {
            $faqs = Faq::where('active', '=', '1')
                ->orderBy('question_fr', 'asc')
                ->get();
        } else
        {
            $faqs = Faq::where('active', '=', '1')
                ->orderBy('question', 'asc')
                ->get();
        }

        return View::make('vcms.faqs')
            ->with('faqs', $faqs)
            ->with('page_title', "FAQs");
    }


    /**
     * List all faqs in admin
     *
     * @return mixed
     */
    public function getAllFaqs()
    {
        $faqs = Faq::orderBy('question', 'asc')
            ->get();

        return View::make('vcms::admin.faqs-all-faqs')
            ->with('faqs', $faqs);
    }


    /**
     * Display Edit or add FAQ
     *
     * @return mixed
     */
    public function editFaq()
    {
        $id = Input::get('id');

        if ($id > 0)
        {
            $faq = Faq::find($id);
        } else
        {
            $faq = new Faq;
        }

        return View::make('vcms::admin.faqs-edit-faq')
            ->with('faq', $faq)
            ->with('faq_id', $id);
    }


    /**
     * Save edited faq
     *
     * @return mixed
     */
    public function postEditFaq()
    {
        $id = Input::get('faq_id');

        if ($id > 0)
        {
            $faq = Faq::find($id);
        } else
        {
            $faq = new Faq;
        }

        $faq->question = Input::get('question');
        $faq->answer = Input::get('answer');

        if (Config::get('vcms::use_french'))
        {
            $faq->question_fr = Input::get('question_fr');
            $faq->answer_fr = Input::get('answer_fr');
        }
        $faq->active = Input::get('active');

        $faq->save();

        return Redirect::to('/admin/faqs/all-faqs')
            ->with('message', 'Changes saved');
    }


    /**
     * Delete a FAQ
     *
     * @return mixed
     */
    public function deleteFaq()
    {
        $id = Input::get('id');
        $faq = Faq::find($id);
        $faq->delete();

        return Redirect::to('/admin/faqs/all-faqs')
            ->with('message', 'FAQ deleted');

    }

}
