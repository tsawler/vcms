<?php namespace verilion\vcms;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Request;

class BlogController extends \Controller {

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }


    /**
     * Get a list of all blogs for admin
     *
     * @return mixed
     */
    public function getAllBlogs()
    {
        $blogs = Blog::orderBy('title')->get();

        return View::make('vcms::admin.blogs-list-all')
            ->with('allblogs', $blogs)
            ->with('page_name', '');
    }


    /**
     * Display individual post
     *
     * @return mixed
     */
    public function getPost()
    {
        $post = BlogPost::where('slug', '=', Request::segment(3))->firstOrFail();
        $archives = BlogPost::archives();

        return View::make('vcms.post')
            ->with('post', $post)
            ->with('archives', $archives);
    }


    /**
     * Get a blog for display
     *
     * @return mixed
     */
    public function getBlog()
    {
        // get blog
        $slug = Request::segment(2);
        $blog = Blog::where('slug', '=', $slug)->firstOrFail();

        // get posts
        $posts = BlogPost::where('blog_id', '=', $blog->id)
            ->where('active', '=', '1')
            ->paginate(10);

        $archives = BlogPost::archives();

        return View::make('vcms.blog')
            ->with('blog', $blog)
            ->with('posts', $posts)
            ->with('archives', $archives);
    }


    /**
     * Delete a blog
     *
     * @return mixed
     */
    public function getDeleteBlog()
    {
        $item = Blog::find(Input::get('id'));
        $item->delete();

        return Redirect::to('/admin/blogs/all-blogs')
            ->with('message', 'Blog deleted');
    }


    /**
     * Display blog for edit, along with all entries
     */
    public function getEditBlog()
    {
        if (Input::get('id') > 0)
        {
            $blog = Blog::find(Input::get('id'));
            $items = BlogPost::where('blog_id', '=', Input::get('id'))->get();
        } else
        {
            $blog = new Blog;
            $items = null;
        }

        return View::make('vcms::admin.blog-edit-blog')
            ->with('blog', $blog)
            ->with('items', $items)
            ->with('blog_id', Input::get('id'));
    }


    /**
     * Display post for edit
     *
     * @return mixed
     */
    public function getEditPost()
    {
        if (Input::get('id') > 0)
        {
            $post = BlogPost::find(Input::get('id'));
        } else
        {
            $post = new BlogPost;
        }

        if (Input::has('src'))
        {
            $src = Input::get('src');
        } else
        {
            $src = '';
        }

        $results = Blog::orderBy('title')->get();
        $blogs = array();

        foreach ($results as $result)
        {
            $blogs[$result->id] = $result->title;
        }

        return View::make('vcms::admin.blog-edit-post')
            ->with('post_id', Input::get('id'))
            ->with('post', $post)
            ->with('blog_id', Input::get('bid'))
            ->with('src', $src)
            ->with('blogs', $blogs);
    }


    /**
     * Save edited post
     *
     * @return mixed
     */
    public function postEditPost()
    {
        if (Input::get('post_id') > 0)
        {
            $post = BlogPost::find(Input::get('post_id'));
        } else
        {
            $post = new BlogPost;
        }

        $post->user_id = Auth::user()->id;
        $post->blog_id = Input::get('blog_id');
        $post->title = Input::get('title');
        $post->summary = Input::get('summary');
        $post->content = Input::get('content');
        if (Config::get('vcms::use_french'))
        {
            $post->title_fr = Input::get('title_fr');
            $post->summary_fr = Input::get('summary_fr');
            $post->content_fr = Input::get('content_fr');
        }
        $post->slug = Str::slug(Input::get('title'));
        $post->meta = "";
        $post->active = Input::get('active');

        if (Input::has('post_date'))
        {
            $post->post_date = Input::get('post_date');
        } else
        {
            $post->post_date = date('Y-m-d');
        }

        $post->save();
        $post_id = $post->id;

        if (Input::hasFile('image_name'))
        {
            $file = Input::file('image_name');
            $destinationPath = base_path() . '/public/blog/' . Input::get('blog_id') . "/";
            $filename = $file->getClientOriginalName();
            $upload_success = Input::file('image_name')->move($destinationPath, $filename);
            if (!\File::exists($destinationPath . "thumbs"))
            {
                \File::makeDirectory($destinationPath . "thumbs");
            }

            $thumb_img = \Image::make($destinationPath . $filename);
            $height = $thumb_img->height();
            $width = $thumb_img->width();


            if (($height < Config::get('vcms::min_img_height')) || ($width < Config::get('vcms::min_img_width')))
            {
                \File::delete($destinationPath . $filename);

                return Redirect::to('/admin/blogs/blog?id=' . Input::get('blog_id'))
                    ->with('error', 'Your image is too small. It must be at least '
                        . Config::get('vcms::min_img_width')
                        . ' pixels wide, and '
                        . Config::get('vcms::min_img_height')
                        . ' pixels tall!');
            }

            $thumb_img->fit(Config::get('vcms::thumb_size'), Config::get('vcms::thumb_size'))
                ->save($destinationPath . "thumbs/" . $filename);

            unset($thumb_img);
            $img = \Image::make($destinationPath . $filename);

            $width = $img->width();

            if ($width > Config::get('vcms::max_blog_img_width'))
            {
                // this image is very large; we'll need to resize it.
                $img = $img->fit(Config::get('vcms::max_blog_img_width'),
                    (Config::get('vcms::max_blog_img_width') * 0.375));
                $img->save();
            }

            if ($upload_success)
            {
                $post = BlogPost::find($post_id);
                $post->image = $filename;
                $post->save();
            }
        }


        if ((Input::has('src')) && (strlen(Input::get('src')) > 0))
        {
            return Redirect::to('/admin/blogs/posts')
                ->with('message', 'Changes saved');
        } else
        {
            return Redirect::to('/admin/blogs/blog?id=' . Input::get('blog_id'))
                ->with('message', 'Changes saved');
        }
    }


    /**
     * Save edited blog
     *
     * @return mixed
     */
    public function postEditBlog()
    {
        $blog_id = Input::get('blog_id');

        if ($blog_id > 0)
        {
            $blog = Blog::find($blog_id);
        } else
        {
            $blog = new Blog;
        }

        $blog->title = Input::get('title');
        if (Config::get('vcms::use_french'))
        {
            $blog->title_fr = Input::get('title_fr');
        }
        $blog->active = Input::get('active');
        $blog->slug = Str::slug(Input::get('title'));
        $blog->save();

        return Redirect::to('/admin/blogs/blog?id=' . $blog->id)
            ->with('message', 'Changes saved');
    }


    /**
     * Save edited post
     *
     * @return mixed
     */
    public function postSave()
    {
        $validator = Validator::make(Input::all(), Post::$rules);
        if ($validator->passes())
        {
            $post = new Post;
            $post->title = trim(Input::get('title'));
            $post->status = Input::get('status');
            $post->published_date = Input::get('post_date') . " 00:00:01";
            $post->content = Input::get('content');
            $post->summary = Input::get('summary');
            $post->meta_description = Input::get('meta_description');
            $post->meta_keywords = Input::get('meta_keywords');
            $post->in_rss = 1;
            $post->slug = urlencode(trim(Input::get('title')));
            $post->save();
            Cache::flush();

            return Redirect::to('/blog')
                ->with('message', 'Post saved successfully');
        } else
        {
            return Redirect::to('post/create')
                ->with('message', 'The following errors occurred')
                ->withErrors($validator)
                ->withInput();
        }
    }


    /**
     * Delete blog post
     */
    public function postDelete()
    {
        $post = Post::find(Input::get('post_id'));
        $post->delete();
        Cache::flush();

        return Redirect::to('/blog')
            ->with('message', 'Post deleted successfully');
    }

}
