<?php  namespace verilion\vcms;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

class VcmsServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred..
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('verilion/vcms');
        if (Config::get('vcms::use_package_routes'))
        {
            include __DIR__ . '/../../routes.php';
        }
        include __DIR__ . '/../../filters.php';
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['vcms'] = $this->app->share(function ($app)
        {
            return new Vcms;
        });
        $this->app->booting(function ()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Vcms', 'verilion\vcms\Facades\Vcms');
        });
        $this->app['config']->package('verilion/vcms', __DIR__ . '/../../config');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('vcms');
    }

}
