<?php namespace verilion\vcms\Facades;

use Illuminate\Support\Facades\Facade;

class Vcms extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'vcms'; }

}