<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VcmsAddEventsTable extends Migration {

    public function up() {
        Schema::create(\Config::get('vcms::events_table'), function($table) {
            $table->increments('id');
            $table->string('event_title');
            $table->string('event_text');
            $table->string('event_title_fr')->nullable();
            $table->string('event_text_fr')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->integer('all_day')->default(0);
            $table->integer('active')->default(0);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop(\Config::get('vcms::events_table'));
    }

}
