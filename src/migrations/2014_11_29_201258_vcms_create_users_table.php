<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VcmsCreateUsersTable extends Migration {

    public function up() {
        Schema::create(\Config::get('vcms::users_table'), function($table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->rememberToken();
            $table->timestamps();
            $table->string('password');
            $table->integer('user_active');
            $table->integer('access_level');
            $table->index('email');
        });
    }

    public function down() {
        Schema::drop(\Config::get('vcms::users_table'));
    }

}
