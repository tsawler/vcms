<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VcmsCreateGalleryTags extends Migration {

    public function up() {
        Schema::create(\Config::get('vcms::gallery_tags_table'), function($table) {
            $table->increments('id');
            $table->string('tag_name');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop(\Config::get('vcms::gallery_tags_table'));
    }

}
