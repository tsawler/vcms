<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VcmsCreateUserRolesTable extends Migration {

    public function up() {
        Schema::create(\Config::get('vcms::user_roles_table'), function($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->string('role');
            $table->timestamps();
            $table->index('user_id');
            $table->index('role_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on(\Config::get('vcms::roles_table'))->onDelete('cascade');
        });
    }

    public function down() {
        Schema::drop(\Config::get('vcms::user_roles_table'));
    }

}
