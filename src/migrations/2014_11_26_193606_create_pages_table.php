<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

    public function up() {
        Schema::create(\Config::get('vcms::pages_table'), function($table) {
            $table->increments('id');
            $table->string('page_title')->unique();
            $table->text('page_content');
            $table->string('page_title_fr')->nullable();
            $table->text('page_content_fr')->nullable();
            $table->timestamps();
            $table->integer('active');
            $table->string('meta')->nullable();
            $table->string('slug');
            $table->string('meta_tags')->nullable();

            $table->index('slug');
        });
    }

    public function down() {
        Schema::drop(\Config::get('vcms::pages_table'));
    }

}
