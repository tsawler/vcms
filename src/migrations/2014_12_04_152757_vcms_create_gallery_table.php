<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VcmsCreateGalleryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up() {
        Schema::create(\Config::get('vcms::galleries_table'), function($table) {
            $table->increments('id');
            $table->string('gallery_name');
            $table->string('gallery_name_fr');
            $table->integer('active');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop(\Config::get('vcms::galleries_table'));
    }

}
