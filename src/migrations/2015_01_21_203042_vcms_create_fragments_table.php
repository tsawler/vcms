<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VcmsCreateFragmentsTable extends Migration {

	public function up() {
		Schema::create(\Config::get('vcms::fragments_table'), function($table) {
			$table->increments('id');
			$table->integer('page_id')->unsigned()->nullable();
			$table->string('fragment_title')->nullable();
			$table->text('fragment_text')->nullable();
			$table->string('fragment_title_fr')->nullable();
			$table->text('fragment_text_fr')->nullable();
			$table->timestamps();

			$table->index('page_id');

			$table->foreign('page_id')
				->references('id')
				->on(\Config::get('vcms::pages_table'))
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}

	public function down() {
		Schema::drop(\Config::get('vcms::fragments_table'));
	}

}
