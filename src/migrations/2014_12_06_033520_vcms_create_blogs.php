<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VcmsCreateBlogs extends Migration {

    public function up() {
        Schema::create(\Config::get('vcms::blogs_table'), function($table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('slug')->unique();
            $table->string('title_fr')->nullable();
            $table->integer('active')->default(0);
            $table->timestamps();
            $table->index('slug');
        });
    }

    public function down() {
        Schema::drop(\Config::get('vcms::blogs_table'));
    }

}
