<?php

/*
|--------------------------------------------------------------------------
| Specific filters
|--------------------------------------------------------------------------
|
|
*/

App::before(function ($request)
{
    if (!Session::has('lang'))
    {
        Session::put('lang', 'en');
    }

    if (Session::get('lang') == "en")
    {
        App::setLocale('en');
    } else
    {
        App::setLocale('fr');
    }

});


Route::filter('auth.admin', function(){
    if (Auth::user())
    {
        if (Auth::user()->access_level < 3)
        {
            return Redirect::guest('login');
        }
    } else {
        return Redirect::guest('login');
    }

});

Route::filter('auth.admin.galleries', function(){
    if (! Auth::user()->hasRole('galleries')){
        return Redirect::to('/admin/unauthorized', 301);
    }
});

Route::filter('auth.admin.blogs', function(){
    if (! Auth::user()->hasRole('blogs')){
        return Redirect::to('/admin/unauthorized', 301);
    }
});

Route::filter('auth.admin.pages', function(){
    if (! Auth::user()->hasRole('pages')){
        return Redirect::to('/admin/unauthorized', 301);
    }
});

Route::filter('auth.admin.events', function(){
    if (! Auth::user()->hasRole('events')){
        return Redirect::to('/admin/unauthorized', 301);
    }
});

Route::filter('auth.admin.users', function(){
    if (! Auth::user()->hasRole('users')){
        return Redirect::to('/admin/unauthorized', 301);
    }
});

Route::filter('auth.admin.news', function(){
    if (! Auth::user()->hasRole('news')){
        return Redirect::to('/admin/unauthorized', 301);
    }
});

Route::filter('auth.admin.faqs', function(){
    if (! Auth::user()->hasRole('faqs')){
        return Redirect::to('/admin/unauthorized', 301);
    }
});
