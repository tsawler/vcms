<?php namespace verilion\vcms;


class BlogPost extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::blog_posts_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * Link to parent blog
     *
     * @return mixed
     */
    public function blog()
    {
        return $this->hasOne('verilion\vcms\Blog', 'id', 'blog_id');
    }

    public function scopeLive($query)
    {
        return $query->where('active', '=', 1)
            ->where('post_date', '<=', date('Y-m-d'));
    }


    /**
     * Get list of archived posts
     *
     * @return array
     */
    public static function archives()
    {
        $query = "
            select distinct on (grouper)
                to_char(post_date, 'YYYY') as year,
                to_char(post_date, 'MM') as month,
                to_char(post_date, 'YYYYMM') as grouper,
                to_char(post_date, 'Month') as month_name,
                  (select count(id) from
                    posts
                    where
                      cast(EXTRACT(MONTH FROM post_date) as integer)
                        = cast(to_char(post_date, 'MM') as integer)
                      and cast(EXTRACT(year from post_date) as integer)
                        = cast(to_char(post_date, 'YYYY') as integer)) as count
            from
                posts
            where
                active = 1
            order by
                grouper, year, month asc
        ";

        $archives = $results = \DB::select($query);

        $results = array();
        foreach ($archives as $archive)
        {
            $results[$archive->year][$archive->month] = array(
                'monthname' => $archive->month_name,
                'count'     => $archive->count,
            );
        }

        return $results;
    }

}
