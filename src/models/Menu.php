<?php namespace verilion\vcms;


class Menu extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::menus_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

}
