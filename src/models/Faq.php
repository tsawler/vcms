<?php namespace verilion\vcms;

class Faq extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::faqs_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

}
