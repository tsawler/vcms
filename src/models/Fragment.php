<?php namespace verilion\vcms;


class Fragment extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::fragments_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


}