<?php namespace verilion\vcms;


class MenuItem extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::menus_items_table');
    }

    public static $rules = array(
        'menu_text' => 'required|min:2'
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * Dropdown items for this menu
     *
     * @return mixed
     */
    public function dropdownItems()
    {
        return $this->hasMany('verilion\vcms\MenuDropdownItem')->orderBy('sort_order');;
    }


    /**
     * Get info from pages table for the record.
     *
     * @return mixed
     */
    public function targetPage()
    {
        return $this->hasOne('verilion\vcms\Page', 'id', 'page_id');
    }
}
