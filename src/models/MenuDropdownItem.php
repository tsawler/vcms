<?php namespace verilion\vcms;

class MenuDropdownItem extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::menu_dropdown_items_table');
    }

    public static $rules = array(
        'menu_text' => 'required|min:2'
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * Get info from pages table for a record.
     *
     * @return mixed
     */
    public function targetPage()
    {
        return $this->hasOne('verilion\vcms\Page', 'id', 'page_id');
    }
}
