<?php namespace verilion\vcms;


class GalleryItem extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::gallery_items_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


}