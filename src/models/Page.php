<?php namespace verilion\vcms;


class Page extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::pages_table');
    }

    protected $guarded = array('*');

    public static $rules = array(
        'page_title' => 'min:2|unique:pages',
        'page_name'  => 'unique:pages'
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    public function fragments()
    {
        return $this->hasMany('verilion\vcms\Fragment', 'page_id', 'id');
    }

}
