<?php namespace verilion\vcms;


class Blog extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::blogs_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * Posts for blog
     *
     * @return mixed
     */
    public function posts()
    {
        return $this->hasMany('verilion\vcms\Posts', 'blog_id', 'id');
    }

}