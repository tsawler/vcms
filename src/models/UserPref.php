<?php namespace verilion\vcms;


class UserPref extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::user_prefs_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id');
    }

}
