<?php namespace verilion\vcms;


class Event extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::events_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


}