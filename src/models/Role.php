<?php namespace verilion\vcms;

use Illuminate\Auth;

/**
 * Class Role
 * @package verilion\vcms
 */
class Role extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::roles_table');
    }


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany('User');
    }

}
