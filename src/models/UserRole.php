<?php namespace verilion\vcms;


class UserRole extends \Eloquent {

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms::user_roles_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * Link to user
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id');
    }


    /**
     * Link to roles
     *
     * @return mixed
     */
    public function role()
    {
        return $this->belongsTo('verilion\vcms\Role', 'role_id', 'id');
    }

}
