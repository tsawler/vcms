<?php namespace verilion\vcms;

class VcmsDefaultTableSeeder extends \Seeder {

    public function run()
    {
        \Eloquent::unguard();

        \DB::table(\Config::get('vcms::pages_table'))->delete();

        Page::create(array(
            'page_title'      => 'Home',
            'page_content'    => 'This is the home page.',
            'page_title_fr'   => 'Home',
            'page_content_fr' => 'This is the home page.',
            'active'          => '1',
            'meta'            => 'meta',
            'slug'            => 'home',
            'meta_tags'       => 'tags'
        ));

        \DB::table(\Config::get('vcms::menus_table'))->delete();

        Menu::create(array(
            'menu_name' => 'Main Menu'
        ));

        \DB::table(\Config::get('vcms::menu_items_table'))->delete();

        MenuItem::create(array(
            'menu_id'      => '1',
            'menu_text'    => 'Home',
            'menu_text_fr' => 'Home',
            'url'          => '',
            'active'       => '1',
            'has_children' => '0',
            'sort_order'   => '1',
            'page_id'      => '1'
        ));

        \DB::table(\Config::get('vcms::roles_table'))->delete();

        Role::create(
            array('id' => '1', 'role_name' => 'Manage Pages', 'role' => 'pages'));
        Role::create(
            array('id' => '2', 'role_name' => 'Manage Calendar Events', 'role' => 'events'));
        Role::create(
            array('id' => '3', 'role_name' => 'Manage Blogs', 'role' => 'blogs'));
        Role::create(
            array('id' => '4', 'role_name' => 'Manage Galleries', 'role' => 'galleries'));
        Role::create(
            array('id' => '5', 'role_name' => 'Manage Users', 'role' => 'users'));
        Role::create(
            array('id' => '6', 'role_name' => 'Manage Menus', 'role' => 'menus'));
        Role::create(
            array('id' => '7', 'role_name' => 'Manage News', 'role' => 'news'));
        Role::create(
            array('id' => '8', 'role_name' => 'Manage FAQs', 'role' => 'faqs'));


        \DB::table('users')->delete();

        \User::create(
            array(
                'id'           => '1',
                'email'        => 'trevor.sawler@me.com',
                'first_name'   => 'Trevor',
                'last_name'    => 'Sawler',
                'password'     => \Hash::make('marlow11'),
                'user_active'  => 1,
                'access_level' => 3
            )
        );

        \DB::table(\Config::get('vcms::user_roles_table'))->delete();

        UserRole::create(
            array('id' => '1', 'user_id' => '1', 'role_id' => '1', 'role' => 'pages'));
        UserRole::create(
            array('id' => '2', 'user_id' => '1', 'role_id' => '2', 'role' => 'calendars'));
        UserRole::create(
            array('id' => '3', 'user_id' => '1', 'role_id' => '3', 'role' => 'blogs'));
        UserRole::create(
            array('id' => '4', 'user_id' => '1', 'role_id' => '4', 'role' => 'galleries'));
        UserRole::create(
            array('id' => '5', 'user_id' => '1', 'role_id' => '5', 'role' => 'users'));
        UserRole::create(
            array('id' => '6', 'user_id' => '1', 'role_id' => '6', 'role' => 'menus'));
        UserRole::create(
            array('id' => '7', 'user_id' => '1', 'role_id' => '7', 'role' => 'news'));
        UserRole::create(
            array('id' => '8', 'user_id' => '1', 'role_id' => '8', 'role' => 'faqs'));

    }

}