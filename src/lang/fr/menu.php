<?php

return array(
    'language_switch' => 'English',
    'lang'            => '/changelanguage?lang=en',
    'news'            => 'Nouvelles',
    'home'            => 'Accueil',
    'events'          => 'Calendrier des &eacute;v&eacute;nements',
    'faqs'            => 'FAQs',
);
