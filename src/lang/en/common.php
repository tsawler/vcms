<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Common
    |--------------------------------------------------------------------------
    */

    'home'                        => 'Home',

    'about'                       => 'About',

    'dashboard'                   => 'Dashboard',

    'partners'                    => 'Partners',

    'contact'                     => 'Contact',

    'videos'                      => 'Videos',

    'logout'                      => 'Logout',

    'read_more'                   => 'Read More',

    'downloads'                   => 'Downloads',

    'follow_us'                   => 'Follow Us',

    'language_switch'             => 'Fran&ccedil;ais',

    'change_lang'                 => '/page/changelanguage?lang=fr',

    'register'                    => 'Apply',

    'previous'                    => '&laquo; Previous',

    'next'                        => 'Next &raquo;',

    /* login stuff */

    "password"                    => "Passwords must be at least six characters and match the confirmation.",

    "user"                        => "We can't find a user with that e-mail address.",

    "token"                       => "This password reset token is invalid.",

    "logged_in"                   => 'You are now logged in',

    "sent"                        => "Password reminder sent!",

    'forgot'                      => 'Forgot your password? Click here to reset it.',

    'you_email'                   => 'you@example.com',

    'confirm_password'            => 'Confirm Password',

    'incorrect_username_password' => 'Your username/password combination is incorrect',

    'logged_out'                  => 'You are now logged out',

);
