<?php

return array(
    'language_switch' => 'Fran&ccedil;ais',
    'lang'            => '/changelanguage?lang=fr',
    'news'            => 'News',
    'home'            => 'Home',
    'events'          => 'Events Calendar',
    'faqs'            => 'FAQs',
);
