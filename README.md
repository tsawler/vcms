vcms
====
Simple in-place content management, events, user management, galleries, and blog for Laravel 4.2.


## Installation

1. Add `repositories` and `require` for verilion/vcms in `composer.json`:

	```php
	"repositories": [
    {
        "type": "vcs",
        "url": "tcs@beta.verilion.com:repo.git/vcms.git"
    }
    ],
    "require": {
        "laravel/framework": "4.2.*",
        "verilion/vcms": "dev-master"
    },
    ```
    
2. Add `VcmsServiceProvider` to `app/config/app.php`:

	```php
        'verilion\vcms\VcmsServiceProvider',
    ),
	```
	
2. Copy necessary methods from verilion/vcms/src/models/VcmsUser.php to app/models/User.php


4. Publish assets:

    `php artisan asset:publish verilion/vcms`
    
5. Publish config:

	`php artisan config:publish verilion/vcms`
	
	Change permissions of `/public/galleries` and `/public/blog` to 777 (or something writable by the web server).

6. Make changes, if necessary, in `app/config/packages/verilion/vcms/config.php`

7. Run migrations:

    `php artisan migrate --package="verilion/vcms"`

8. Seed datatabases:

    `php artisan db:seed --class=VcmsSeeder`
    
9. Copy methods from User in vcms to models/User.php
    

 

## Content Management


## User Management

## Galleries


## Blog



Event Calendar
--------------
